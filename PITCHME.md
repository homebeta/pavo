# Un Pavo Real en el reino de los pinguinos

---

## Los pinguinos dominaban

![](assets/img/pinguinos.jpg)

---
@title[Customize Slide Layout]

@snap[west span-50]
## Pinguinos
@snapend

@snap[east span-50]
![](assets/img/presentation.png)
@snapend

---?color=#E58537
@title[Add A Little Imagination]

@snap[north-west]
#### Que pasaba con las aves que deseaban  @color[cyan](**ascender**), tenian que:
@snapend

@snap[west span-55]
@ul[spaced text-white]
- Los mas parecido a posible
- Caminar con pasos cortos
- Imitar manera de andar
- Usar traje de pinguino
- Seguir ejemplo de sus jefes
@ulend
@snapend

---?color=#E52567
@title[Add A Little Imagination]

@snap[north-west]
#### Se daba por sentado que todos los pinguinos eran:
@snapend

@snap[west span-55]
@ul[spaced text-white]
- Jefes naturales
- Ordenados
- Leales
- Podian trabajar en equipo
- Anteponian los intereses de la empresa ante asuntos personales
@ulend
@snapend

@snap[east span-45]
@img[shadow](assets/img/conference.png)
@snapend

---?image=assets/img/pavo.jpg
@snap[north span-100 headline]
## Llega Pedro al Reino de los Pinguinos
@snapend


---?color=#E59787
@title[Add A Little Imagination]

@snap[north-west]
#### Pedro venía del **Reino del Aprendizaje** con Lema:**(Imagina, Prueba, Realiza!)** donde, habian:
@snapend

@snap[west span-55]
@ul[spaced text-white]
- Aves Sabias (Los buhos)
- Poderosas (Las Águilas)
- Aves de Caza (Los Halcones)
- Aves raras(avestruces)
- Elegantes (Cisnes)
- Extrañas (Pájaros bobos)
@ulend
@snapend

---
@title[Customize Slide Layout]

@snap[west span-50]
#### Pedro recibe la presión de los pinguinos mayores, insisten
@snapend

---?color=#E59737
@title[Add A Little Imagination]

@snap[west span-65]
@ul[spaced text-white]
- Por qué no te pintas las plumas de negro? Como nosotros
- Qué tiene de malo que yo sea así? -- Se preguntó Pedro
- Comportate como nosotros -- insistieron 
- Ponte un traje de pinguino
- Camina como nosotros
- Por qué simplemente no puedo ser como soy? -- exclamó
@ulend
@snapend


@snap[east span-50]
## Muchas aves pasaban x lo mismo
@snapend

---?color=#E59888
@title[Add A Little Imagination]

@snap[north-east]
#### Por ejemplo, varios usaban vestido de pinguino:
@snapend

@snap[west span-65]
@ul[spaced text-white]
- Eduardo el Aguila: **inteligente** y lo usaba x obligación
- Helena la gavilana: Hermosa y **llena de energía;inteligente, aguda** y agresiva.
- Miguel el pajaro burlón: **Brillante, creativo**, imaginativo, impulsivo
- Sara el cisne: **Soñadora optimista**
@ulend
@snapend

---?image=assets/img/presenter.jpg

@snap[north span-100 headline]
## Video
[[https://www.melflix.com/product/02311/un-pavo-real-en-la-tierra-de-los-pinguinos]]
@snapend

@snap[south span-100 text-06]

@snapend
